import { useSelector } from "react-redux"
import { useContext, useEffect, useState } from "react"
import AuthContext from "../context/AuthContext"
import { setGameGenresActions, setGameInfoActions, setIsFavoriteActions } from "../store/gameInfoReducer"
import { setGamesActions } from "../store/catalogMenuReducer"
import { setGenresActions } from "../store/catalogFilterReducer"
import { setUserInfoActions } from "../store/userReducer"
import { setFavoritesActions } from "../store/profileReducer"

const base_url = 'http://127.0.0.1:8000/api/'

function AccessToken() {
    let {authTokens} = useContext(AuthContext)
    return authTokens.access
}

function SendAuthGet(url, token) {
    return fetch(base_url + url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
    }).then(res => res.json())
}

function SendAuth(data, url, method, token) {
    if (method == 'GET') {
        return SendAuthGet(url, token)
    } 
    return fetch(base_url + url, {
        method: method,
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(data)
    }).then(res => res.json())
}

export const GetFavoriteList = (data, dispatch, token) => {
    return SendAuth(data, 'favorite_list/', 'POST', token).then(res => {
        console.log('222', res)
        dispatch(setFavoritesActions(res))
    })
}
export const IsFavorite = (data, dispatch, token) => {
    return SendAuth(data, 'is_favorite/', 'POST', token).then(res => {
        dispatch(setIsFavoriteActions(res.length >= 1))
    })
}
export const AddFavorite = (data, token) => {return SendAuth(data, 'favorite_detail/', 'POST', token)}
export const DeleteFavorite = (data, token) => {
    return fetch(base_url + 'favorite_detail/', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(data)
    })
}
export const GetUserInfo = (user_id, dispatch, token) => {
    return SendAuth({}, 'user_detail/' + user_id, 'GET', token).then(res => {
        dispatch(setUserInfoActions(res.avatar, res.username))
    })
}
export const UpdateUserInfo = (data, user_id, token) => {
    return fetch(base_url + 'user_detail/' + user_id, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: data
    })
}

function SendGet(url) {
    return fetch(base_url + url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
    }).then(res => res.json())
}

function Send(data, url, method) {
    if (method == 'GET') {
        return SendGet(url)
    } 
    return fetch(base_url + url, {
        method: method,
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(data)
    }).then(res => res.json())
}

export async function CreateUser (data) {
    return await fetch(base_url + 'user_create/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(data)
    })
}

export const GetGameInfo = (game_id, dispatch) => {
    return Send({}, 'game_detail/' + game_id, 'GET').then(res => {
        dispatch(setGameInfoActions(res))
    })
}
export const GetGameList = (data, dispatch) => {
    return Send(data, 'game_list/', 'POST').then(res => {
        dispatch(setGamesActions(res))
    })
}
export const GetGenreList = (game_id, dispatch) => {
    return Send({game_id:game_id}, 'genre_list/', 'POST').then(res => {
        dispatch(setGameGenresActions(res.map(elem => elem.genre)))
    })
}
export const GetAllGenres = (dispatch) => {
    return Send({}, 'all_genres/', 'GET').then(res => {
        dispatch(setGenresActions(res.map(elem => elem.genre)))
    })
}

export const GetImage = (url) => {
    return 'http://127.0.0.1:8000' + url
}
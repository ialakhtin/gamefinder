const defaultState = {
    favorites: []
}

const SET_FAVORTES = 'SET_FAVORITES'

export const profileReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_FAVORTES:
            console.log('111', action.favorites)
            return {...state, favorites: action.favorites}
        default:
            return state
    }
}

export const setFavoritesActions = (favorites) => ({type: SET_FAVORTES, favorites})

import {combineReducers, createStore} from 'redux'
import { userReducer } from './userReducer'
import {menuReducer} from './menuReducer'
import { catalogReducer } from './catalogReducer'
import { authReducer } from './authReducer'
import { profileReducer } from './profileReducer'
import { gameInfoReducer } from './gameInfoReducer'
import { genreInfoReducer } from './genreInfoReducer'

const rootReducer =  combineReducers({
    user: userReducer,
    menu: menuReducer,
    catalog: catalogReducer,
    auth: authReducer,
    profile: profileReducer,
    game_info: gameInfoReducer,
    genre_info: genreInfoReducer
})

const persistedState = localStorage.getItem('reduxState') 
                       ? JSON.parse(localStorage.getItem('reduxState'))
                       : {}

var state = {}
try {
    state = { 
        user: persistedState.user, 
        profile: persistedState.profile,
        game_info: persistedState.game_info,
        genre_info: persistedState.genre_info
    }
} catch(error) {
    
}
export const store = createStore(rootReducer, state)

store.subscribe(()=>{
    localStorage.setItem('reduxState', JSON.stringify(store.getState()))
  })
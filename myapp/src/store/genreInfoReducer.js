const defaultState = {
    genre: ''
}

const SET_GENRE = 'SET_GENRE'

export const genreInfoReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_GENRE:
            return {...state, genre: action.genre}
        default:
            return state
    }
}

export const setCurrentGenreActions = (genre) => ({type: SET_GENRE, genre})

import {combineReducers} from 'redux'
import { catalogFilterReducer } from "./catalogFilterReducer";
import { catalogMenuReducer } from "./catalogMenuReducer";

export const catalogReducer =  combineReducers({
    menu: catalogMenuReducer,
    filter: catalogFilterReducer
})
const defaultState = {
    name: '',
    genres: [],
    show_all: false,
    choosen_genres: [],
    player_count: {start: 1000, end: -1},
    play_time: {start: 1000, end: -1},
    rating: {start: -1, end: -1}
}

const SET_NAME = 'FILTER_SET_GAME_NAME'
const SHOW = 'GENRES_SHOW'
const CHANGE_SHOW = 'CHANGE_GENRES_SHOW'
const SET_GENRES = 'SET_ALL_GENRES'
const SET_CHOOSEN_GENRES = 'SET_CHOOSEN_GENRES'
const SET_PLAYER_COUNT = 'SET_PLAYER_COUNT'
const SET_PLAY_TIME = 'SET_PLAY_TIME'
const SET_RATING = 'SET_RATING'
const TO_DEFAULT = 'TO_DEFAULT_FILTERS'

export const catalogFilterReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_NAME:
            return {...state, name: action.name}
        case SHOW:
            return {...state, show_all: action.show_all}
        case CHANGE_SHOW:
            return {...state, show_all: !state.show_all}
        case SET_GENRES:
            return {...state, genres: action.genres,
                choosen_genres: Array(action.genres.length).fill(0)}
        case SET_CHOOSEN_GENRES:
            if (typeof action.index == 'string') {
                action.index = state.genres.indexOf(action.index)
            }
            if (action.index < 0) return state
            state.choosen_genres[action.index] = action.value
            return {...state}
        case SET_PLAYER_COUNT:
            return {...state, player_count: {start: action.start, end: action.end}}
        case SET_PLAY_TIME:
            return {...state, play_time: {start: action.start, end: action.end}}
        case SET_RATING:
            return {...state, rating: {start: action.start, end: action.end}}
        case TO_DEFAULT:
            return {
                ...state,
                name: '',
                choosen_genres: Array(state.genres.length).fill(0),
                player_count: {start: 1000, end: -1},
                play_time: {start: 1000, end: -1}
            }
        default:
            return state
    }
}

export const setFilterNameActions = (name) => ({type: SET_NAME, name})
export const showGenresActions = (show_all) => ({type: SHOW, show_all})
export const changeShowGenresActions = () => ({type: CHANGE_SHOW})
export const setGenresActions = (genres) => ({type: SET_GENRES, genres})
export const setChoosenGenresActions = (index, value) => ({type: SET_CHOOSEN_GENRES,
    index, value})
export const setPlayerCountActions = (start, end) => ({type: SET_PLAYER_COUNT, start, end})
export const setPlayTimeActions = (start, end) => ({type: SET_PLAY_TIME, start, end})
export const setRatingActions = (start, end) => ({type: SET_RATING, start, end})
export const toDefaultFilterActions = () => ({type: TO_DEFAULT})
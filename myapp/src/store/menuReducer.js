const defaultState = {
    shown: false
}

const SHOW = 'SHOW'
const HIDE = 'HIDE'
const CHANGE = 'CHANGE'

export const menuReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW:
            return {...state, shown: true}
        case HIDE:
            return {...state, shown: false}
        case CHANGE:
            return {...state, shown: !state.shown}
        default:
            return state
    }
}

export const showMenuActions = () => ({type: SHOW})
export const hideMenuActions = () => ({type: HIDE})
export const changeMenuActions = () => ({type: CHANGE})

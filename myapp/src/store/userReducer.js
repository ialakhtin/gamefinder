const defaultState = {
    user_image: null,
    user_name: 'user123456'
}

const IMG = 'IMG'
const NAME = 'NAME'
const SET_USER_INFO = 'SET_USER_INFO'

export const userReducer = (state = defaultState, action) => {
    switch (action.type) {
        case IMG:
            return {...state, user_image: action.user_image}
        case NAME:
            return {...state, user_name: action.user_name}
        case SET_USER_INFO:
            return {...state, user_image: action.img, user_name: action.name}
        default:
            return state
    }
}

export const setImageActions = (user_image) => ({type: IMG, user_image})
export const setNameActions = (user_name) => ({type: NAME, user_name})
export const setUserInfoActions = (img, name) => ({type: SET_USER_INFO, img, name})

const defaultState = {
    message: ''
}

const SET_MESSAGE = 'SET_AUTH_MESSAGE'

export const authReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_MESSAGE:
            return {...state, message: action.message}
        default:
            return state
    }
}

export const setMessageActions = (message) => ({type: SET_MESSAGE, message})

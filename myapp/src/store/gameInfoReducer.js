const defaultState = {
    id: 0,
    genres: [],
    title: '',
    icon: '',
    text: '',
    is_favorite: false
}

const SET_ID = 'SET_GAME_ID'
const SET_GAME_INFO = 'SET_GAME_INFO'
const SET_GENRES = 'SET_GAME_GENRES'
const SET_IS_FAVORITE = 'SET_IS_FAVORITE'

export const gameInfoReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_ID:
            return {...state, id: action.id}
        case SET_GAME_INFO:
            return {...state, ...action.info, genres: state.genres}
        case SET_GENRES:
            return {...state, genres: action.genres}
        case SET_IS_FAVORITE:
            return {...state, is_favorite: action.is_favorite}
        default:
            return state
    }
}

export const setGameIdActions = (id) => ({type: SET_ID, id})
export const setGameInfoActions = (info) => ({type: SET_GAME_INFO, info})
export const setGameGenresActions = (genres) => ({type: SET_GENRES, genres})
export const setIsFavoriteActions = (is_favorite) => ({type: SET_IS_FAVORITE, is_favorite})

const defaultState = {
    orders: [],
    current_order: '',
    show_all: false,
    games: []
}

const SHOW = 'ORDER_SHOW'
const ALL = 'ALL'
const CURRENT = 'CURRENT'
const CHANGE_SHOW = 'CHANGE_ORDERS_SHOW'
const SET_GAMES = 'SET_GAMES'

export const catalogMenuReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW:
            return {...state, show_all: action.show_all}
        case CHANGE_SHOW:
            return {...state, show_all: !state.show_all}
        case ALL:
            return {...state, orders: action.orders}
        case CURRENT:
            return {...state, current_order: action.current}
        case SET_GAMES:
            return {...state, games: action.games}
        default:
            return state
    }
}

export const showOrdersActions = (show_all) => ({type: SHOW, show_all})
export const changeShowOrdersActions = () => ({type: CHANGE_SHOW})
export const setOrdersActions = (orders) => ({type: ALL, orders})
export const setCurrentOrderActions = (current) => ({type: CURRENT, current})
export const setGamesActions = (games) => ({type: SET_GAMES, games})

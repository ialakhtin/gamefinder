import { createContext, useState, useEffect } from "react"
import jwt_decode from "jwt-decode"
import { CreateUser, GetUserInfo } from "../services/BackApi"
import { useDispatch } from "react-redux"

const AuthContext = createContext()

export default AuthContext

export const AuthProvider = ({children}) => {
    let [authTokens, setAuthTokens] = useState(JSON.parse(localStorage.getItem('authTokens')))
    let [user, setUser] = useState(()=> localStorage.getItem('authTokens') ? jwt_decode(localStorage.getItem('authTokens')) : null)
    let [loading, setLoading] = useState(true)
    const dispatch = useDispatch()


    const loginUser = async (user_data) => {
        let res = await fetch('http://127.0.0.1:8000/auth_api/token',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(user_data)
        })

        let data = await res.json()
        const result = res.status == 200
        console.log(result)
        if (result){
            setAuthTokens(data)
            setUser(jwt_decode(data.access))
            localStorage.setItem('authTokens', JSON.stringify(data))
        } else {
            console.log('error')
        }

        if(loading){
            setLoading(false)
        }
        return result
    }

    const registerUser = async (user_data) => {
        let res = await CreateUser(user_data)

        let data = await res.json()
        const result = res.status == 200
        console.log(result)
        if (result){
            setAuthTokens(data)
            setUser(jwt_decode(data.access))
            localStorage.setItem('authTokens', JSON.stringify(data))
        } else {
            console.log('error')
        }

        if(loading){
            setLoading(false)
        }
        return result
    }

    const logoutUser = () => {
        setAuthTokens(null)
        setUser(null)
        localStorage.removeItem('authTokens')
    }

    let updateToken = async ()=> {
        let res = await fetch('http://127.0.0.1:8000/auth_api/refresh',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({refresh: authTokens?.refresh})
        })

        let data = await res.json()

        if (res.status == 200){
            GetUserInfo(user.user_id, dispatch, data.access)
            data.refresh = authTokens.refresh
            setAuthTokens(data)
            setUser(jwt_decode(data.access))
            localStorage.setItem('authTokens', JSON.stringify(data))
        }else{
            logoutUser()
        }
    }

    let contextData = {
        user: user,
        authTokens: authTokens,
        loginUser: loginUser,
        logoutUser: logoutUser,
        registerUser: registerUser
    }

    useEffect(()=> {
        if(loading){
            setLoading(false)
            updateToken()
        }
    
        let fourMinutes = 1000 * 60 * 4

        let interval =  setInterval(()=> {
            if(authTokens){
                updateToken()
            }
        }, fourMinutes)
        return ()=> clearInterval(interval)

    }, [authTokens, loading])


    return (
    <AuthContext.Provider value={contextData} >
        {children}
    </AuthContext.Provider>
    )
}
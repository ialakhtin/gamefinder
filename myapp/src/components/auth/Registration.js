import { useDispatch } from "react-redux";
import BaseAuth from "./BaseAuth";
import { setMessageActions } from "../../store/authReducer"
import { CreateUser } from "../../services/BackApi";
import { useContext } from "react";
import AuthContext from "../../context/AuthContext";

export default function Registration() {
    const dispatch = useDispatch()
    const {registerUser} = useContext(AuthContext)
    const handler = (login, password, double_pass) => {
        if (password != double_pass) {
            dispatch(setMessageActions('Пароли не совпадают'))
        } else {
            let data = {
                username: login,
                password: password
            }
            registerUser(data).then(res => {
                console.log(res)
                if (res) {
                    dispatch(setMessageActions(''))
                    window.location.assign('/catalog')
                } else {
                    dispatch(setMessageActions('Пользователь с таким именем уже есть'))
                }
            })
        }
    }

    return (
        <BaseAuth title='Регистрация' handler={handler} need_double={true}/>
    )
}
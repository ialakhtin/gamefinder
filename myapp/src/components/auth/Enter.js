import { useContext } from "react";
import { useDispatch } from "react-redux";
import AuthContext from "../../context/AuthContext";
import { setMessageActions } from "../../store/authReducer";
import BaseAuth from "./BaseAuth";

export default function Enter() {
    const {loginUser} = useContext(AuthContext)
    const dispatch = useDispatch()
    const handler = (login, password, double_pass) => {
        const data = {
            username: login,
            password: password
        }
        loginUser(data).then((res) => {
            if (res) {
                dispatch(setMessageActions(''))
                window.location.assign('/catalog')
            } else {
                dispatch(setMessageActions('Неверный логин или пароль!'))
            }
        })
    }

    return (
        <BaseAuth title='Вход' handler={handler} need_double={false}/>
    )
}
import { useSelector } from "react-redux"
import "../../styles/auth.css"

function DoublePass(props) {
    if (!props.need_double)
        return (<div></div>)
    return <input onKeyUp={props.handler} type="password" placeholder='Повторите пароль'/>
}

var login = ''
var password = ''
var double_pass = ''

export default function BaseAuth(props) {
    const message = useSelector(state => state.auth.message)
    const handleLogin = (event) => { login = event.target.value }
    const handlePass = (event) => { password = event.target.value; }
    const handleDouble = (event) => { double_pass = event.target.value }
    const buttonHandler = () => {
        props.handler(login, password, double_pass)
    }
    return (
        <div className="authorisation">
            <h1>{props.title}</h1>
            <input onKeyUp={handleLogin} type='text' placeholder='Логин'/>
            <input onKeyUp={handlePass} type="password" placeholder='Пароль'/>
            <DoublePass need_double={props.need_double} handler={handleDouble}/>
            <p>{message}</p>
            <button onClick={buttonHandler}>{props.title}</button>
        </div>
    )
}
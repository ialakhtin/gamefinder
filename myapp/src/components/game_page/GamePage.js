import { useContext, useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import AuthContext from "../../context/AuthContext"
import { AddFavorite, DeleteFavorite, GetGameInfo, GetGenreList, IsFavorite } from "../../services/BackApi"
import { setIsFavoriteActions } from "../../store/gameInfoReducer"
import { setCurrentGenreActions } from "../../store/genreInfoReducer"
import "../../styles/game_page.css"

function FavoriteButton(props) {
    if (props.is_favorite) 
        return (
            <button className="favorite_button" id="del_from_favorite" onClick={props.deleteHandler}>
                Удалить из избранного
            </button>
        )
    return (
        <button className="favorite_button" id="add_to_favorite" onClick={props.addHandler}>
            Добавить в избранное
        </button>
    )
}

function GameGenre(props) {
    const dispatch = useDispatch()
    return (
        <a onClick={() => {dispatch(setCurrentGenreActions(props.name))}}
            href='/catalog' className="game_genre">
            {props.name}
        </a>
    )
}

function GameGenreList(props) {
    return (
        <div className="game_genre_list">
            <h3>Жанры</h3>
            {props.genres.map(elem => 
                <GameGenre key={'game_genre_' + elem} name={elem}/>)}
        </div>
    )
}

export default function GamePage() {
    const dispatch = useDispatch()
    const game_id = useSelector(state => state.game_info.id)
    const {user, authTokens} = useContext(AuthContext)
    const info = useSelector(state => state.game_info)
    let data = {game_id: game_id}
    if (user) {
        data.user_id = user.user_id
    }
    useEffect(() => {
        GetGameInfo(game_id, dispatch)
        GetGenreList(game_id, dispatch)
        if (user) IsFavorite(data, dispatch, authTokens.access)
    }, [])
    const deleteHandler = () => {
        if (!user) return
        DeleteFavorite(data, authTokens.access)
        dispatch(setIsFavoriteActions(false))
    }
    const addHandler = () => {
        if (!user) return
        AddFavorite(data, authTokens.access)
        dispatch(setIsFavoriteActions(true))
    }
    return (
        <div className="game_page">
            <div>
                <div className="image_container"><img src={info.icon}/></div>
                <FavoriteButton deleteHandler={deleteHandler} addHandler={addHandler}
                    is_favorite={info.is_favorite}/>
            </div>
            <div className="game_info">
                <h2>{info.name}</h2>
                <div>
                    <h3>Описание</h3>
                    <p>{info.text}</p>
                    <GameGenreList genres={info.genres}/>
                </div>
            </div>
        </div>
    )
}
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import '../styles/common.css'
import HeaderWithMenu from './common/HeaderWithMenu';
import Catalog from "./catalog/Catalog"
import Enter from "./auth/Enter"
import Registration from "./auth/Registration"
import Profile from "./profile/Profile"
import GamePage from "./game_page/GamePage"
import Settings from "./settings/Settings"
import { AuthProvider } from "../context/AuthContext"

function App() {
  return (
    <AuthProvider>
      <div className="main_wrapper">
        <HeaderWithMenu/>
        <Router>
          <Routes>
            <Route path='/catalog' element={<Catalog/>}/>
            <Route path='/enter' element={<Enter/>}/>
            <Route path='/registration' element={<Registration/>}/>
            <Route path='/profile' element={<Profile/>}/>
            <Route path='/game_page' element={<GamePage/>}/>
            <Route path='/settings' element={<Settings/>}/>
          </Routes>
        </Router>
      </div>
    </AuthProvider>
  );
}

export default App;

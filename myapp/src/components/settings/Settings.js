import { useSelector, useDispatch } from "react-redux"
import { useContext, useState } from "react"
import { setNameActions, setUserInfoActions } from "../../store/userReducer"
import "../../styles/settings.css"
import { GetUserInfo, UpdateUserInfo } from "../../services/BackApi"
import AuthContext from "../../context/AuthContext"

export default function Settings() {
    const {user, authTokens} = useContext(AuthContext)
    const user_image = useSelector(state => state.user.user_image)
    const user_name = useSelector(state => state.user.user_name)
    const [name, setName] = useState(user_name)
    const [image, setImage] = useState(null)
    const [shownImage, setShownImage] = useState(user_image)
    const [message, setMessage] = useState('')
    const dispatch = useDispatch()
    const handlerName = (event) => {
        setName(event.target.value)
    }
    const handlerImage = (event) => {
        setImage(event.target.files[0])    
        setShownImage(URL.createObjectURL(event.target.files[0]))
    }
    const buttonHandler = () => {
        const data = new FormData()
        data.append('username', name)
        if (image) data.append('avatar', image, image.name)
        console.log(data)
        UpdateUserInfo(data, user.user_id, authTokens.access).then(res => {
            if (res.status != 200) {
                setMessage('Пользователь с таким именем уже есть')
            } else {
                GetUserInfo(user.user_id, dispatch, authTokens.access)
            }
        })
    }   
    return (
        <div className="settings">
            <h2>Настройки</h2>
            <div>
                <div className="settings_wrapper">
                    <div><img src={image ? URL.createObjectURL(image) : user_image}/></div>
                    <label>
                        <input onChange={handlerImage} type="file" name="file" accept="image/*"/>		
                        <span>изменить аватар</span>
                    </label>                
                </div>
                <input type='text' onChange={handlerName} value={name}/>
            </div>
            <p>{message}</p>
            <button onClick={buttonHandler}>Сохранить</button>
        </div>
    )   
}
import { useSelector } from "react-redux"
import FavoriteItem from "./FavoriteItem"

export default function FavoriteList() {
    const favorites = useSelector(state => state.profile.favorites)
    console.log(favorites)
    return (
        <div className="favorite_list">
            <h3>Избранное</h3>
            {favorites.map(elem => 
                <FavoriteItem key={'favorite_' + elem.id} game_info={elem} />) }
        </div>
    )
}
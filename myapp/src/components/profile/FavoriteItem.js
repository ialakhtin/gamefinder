import { setGameIdActions } from "../../store/gameInfoReducer"
import { useDispatch } from "react-redux"
import { DeleteFavorite, GetFavoriteList, GetImage } from "../../services/BackApi";
import { useContext } from "react";
import AuthContext from "../../context/AuthContext";

export default function FavoriteItem(props) {
    const info = props.game_info
    const dispatch = useDispatch();
    const {user, authTokens} = useContext(AuthContext)
    const handler = () => {
        let data = {
            user_id: user.user_id,
            game_id: info.id
        }
        DeleteFavorite(data, authTokens.access).then(() => {
            GetFavoriteList({user_id: user.user_id}, dispatch, authTokens.access)
        })
    }
    return (
        <div className="favorite_item">
            <div><img src={GetImage(info.icon)}/></div>
            <a onClick={() => dispatch(setGameIdActions(info.id))} 
                href='/game_page'>{info.title}</a>
            <button onClick={handler}>Удалить из избранного</button>
        </div>
    )
}
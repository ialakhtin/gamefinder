import { useDispatch, useSelector } from "react-redux"
import FavoriteList from "./FavoritesList"
import "../../styles/profile.css"
import { useContext, useEffect } from "react"
import { GetFavoriteList } from "../../services/BackApi"
import AuthContext from "../../context/AuthContext"

export default function Profile() {
    const user_image = useSelector(state => state.user.user_image)
    const user_name = useSelector(state => state.user.user_name)
    const dispatch = useDispatch()
    const {user, authTokens} = useContext(AuthContext)
    useEffect(() => {
        GetFavoriteList({user_id: user.user_id}, dispatch, authTokens.access)
    }, [])
    return (
        <div className="profile">
            <div className="profile_header">
                <div><img src={user_image}/></div>
                <h2>{user_name}</h2>
                <a href='/settings'>настройки</a>
            </div>
            <FavoriteList/>
        </div>
    )
}
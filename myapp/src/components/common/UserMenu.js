import '../../styles/user_menu.css'
import {useSelector} from 'react-redux'
import { useContext } from 'react'
import AuthContext from '../../context/AuthContext'

export default function UserMenu() {
    const shown = useSelector(state => state.menu.shown)
    const user_id = useSelector(state => state.user.user_id)
    const {logoutUser} = useContext(AuthContext)
    if (!shown)
        return (
            <div></div>
        )
    var path = '/user/' + user_id + '/'
    return (
        <div className="menu" id='user_menu'>
            <a href={'/profile'}>Профиль</a>
            <hr id='hr_line'/>
            <a href='/settings'>Настройки</a>
            <a href='/enter' onClick={logoutUser}>Выход</a>
        </div>
    )
}
import { useDispatch, useSelector } from "react-redux"
import { useState } from "react"
import { setChoosenGenresActions } from "../../store/catalogFilterReducer"
import accept from "../../data/images/choose_button/accept.png"
import decline from "../../data/images/choose_button/decline.png"
import basic from "../../data/images/choose_button/basic.png"

const states = [basic, accept, decline]

export default function ChooseGenreButton(props) {
    const dispatch = useDispatch()
    var g_state = useSelector(state => state.catalog.filter.choosen_genres)
    const image = states[g_state[props.index]]
    var [image_state, setState] = useState(image)
    const handler = () => {
        dispatch(setChoosenGenresActions(props.index, (g_state[props.index] + 1) % states.length))
        setState(states[g_state[props.index]])
    }
    return (
        <button onClick={handler} className="choose_button">
            <img src={image_state}/>
            <div>{props.text}</div>
        </button>
    )
}
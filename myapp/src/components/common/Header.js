import "../../styles/header.css"
import catalog_img from '../../data/images/catalog.png'
import faq_img from '../../data/images/faq.png'
import notify_img from '../../data/images/notification.png'
import { useDispatch, useSelector } from "react-redux"
import {changeMenuActions} from "../../store/menuReducer"
import { useContext } from "react"
import AuthContext from "../../context/AuthContext"

function UserInfo() {
    const dispatch = useDispatch();
    const user_img = useSelector(state => state.user.user_image)
    return (
        <div className='account_info'>
            <button onClick={() => dispatch(changeMenuActions())}>
                <img src={user_img} id='avatar'/>
            </button>
        </div>
    )
}

function Autorisation() {
    return (
        <div className='account_info'>
            <a href='/enter' className="auth_div">Вход</a>
            <a href='/registration' className="auth_div">Регистрация</a>
        </div>
    )
}

function AccountInfo() {
    const {user} = useContext(AuthContext)
    if (user) {
        return <UserInfo/>;
    } else {
        return <Autorisation/>;
    }
}

export default function Header() {
    return (
        <div className='header'>
            <div>
                <a href='/catalog' id='logo'>Game Tinder</a>
                <a href='/catalog'>
                    <img src={catalog_img}/>
                    Каталог игр
                </a>
            </div>
            <AccountInfo/>
        </div>
    )
}
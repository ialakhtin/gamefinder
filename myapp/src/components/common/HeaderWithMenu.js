import Header from "./Header"
import UserMenu from "./UserMenu"

export default function HeaderWithMenu() {
    return (
        <div>
            <Header/>
            <UserMenu/>
        </div>
    )
}
import FilterGenres from "./FilterGenres";
import { setPlayerCountActions, setPlayTimeActions, setRatingActions, toDefaultFilterActions } 
    from "../../store/catalogFilterReducer"
import FilterRange from "./FilterRange";
import { useDispatch, useSelector } from "react-redux";
import { GetGameList } from "../../services/BackApi";

export default function CatalogFilters() {
    const dispatch = useDispatch()
    const filter = useSelector(state => state.catalog.filter)
    return (
        <div className="catalog_filters">
            <p className="catalog_title">Фильтры</p>
            <FilterGenres/>
            <FilterRange title='Число игроков' action={setPlayerCountActions}/>
            <FilterRange title='Продолжительность' action={setPlayTimeActions}/>
            <FilterRange title='Рейтинг' action={setRatingActions}/>
            <button onClick={() => GetGameList(filter, dispatch)}>Выбрать</button>
            <button onClick={() => {
                const new_filter = {
                    ...filter,
                    name: '',
                    choosen_genres: Array(filter.genres.length).fill(0),
                    player_count: {start: 1000, end: -1},
                    play_time: {start: 1000, end: -1}
                }
                dispatch(toDefaultFilterActions())
                GetGameList(new_filter, dispatch)
            }}>Сбросить</button>
        </div>
    )
}
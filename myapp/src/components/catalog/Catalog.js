import CatalogFilters from "./CatalogFilters";
import CatalogMenu from "./CatalogMenu";
import "../../styles/catalog.css"
import { useDispatch, useSelector } from "react-redux";
import { GetAllGenres, GetGameList } from "../../services/BackApi";
import { useEffect } from "react";
import { setChoosenGenresActions } from "../../store/catalogFilterReducer";
import { setCurrentGenreActions } from "../../store/genreInfoReducer";

export default function Catalog() {
    const dispatch = useDispatch()
    const genre = useSelector(state => state.genre_info.genre)
    const filters = useSelector(state => state.catalog.filter)
    useEffect(() => {
        GetAllGenres(dispatch).then(() => {
            if (genre != '')
                dispatch(setChoosenGenresActions(genre, 1))
            GetGameList(filters, dispatch)
        }).then(() => dispatch(setCurrentGenreActions('')))
    }, [])
    return (
        <div className="catalog">
            <CatalogMenu/>
            <CatalogFilters/>
        </div>
    )
}
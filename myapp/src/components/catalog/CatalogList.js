import { useSelector } from "react-redux"
import CatalogItem from "./CatalogItem"

export default function CatalogList() {
    const games = useSelector(state => state.catalog.menu.games)
    return (
        <div className="catalog_list">
            {games.map(elem => <CatalogItem key={elem.id} name={elem.title}
                image={elem.icon} game_id={elem.id}/>)}
        </div>
    )
}
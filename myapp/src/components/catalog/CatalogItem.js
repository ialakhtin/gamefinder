import { setGameIdActions } from "../../store/gameInfoReducer"
import { useDispatch } from "react-redux"
import { GetImage } from "../../services/BackApi"

export default function CatalogItem(props) {
    const dispatch = useDispatch()
    const image = GetImage(props.image)
    return (
        <a onClick={() => dispatch(setGameIdActions(props.game_id))}
            href='/game_page' className="catalog_item">
            <div>
                <img src={image}/>
            </div>
            <p>{props.name}</p>
        </a>
    )
}
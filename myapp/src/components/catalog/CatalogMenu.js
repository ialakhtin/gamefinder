import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetGameList } from "../../services/BackApi";
import { setFilterNameActions } from "../../store/catalogFilterReducer";
import CatalogList from "./CatalogList";
import CatalogOrder from "./CatalogOrder";

export default function CatalogMenu() {
    const dispatch = useDispatch()
    const filter = useSelector(state => state.catalog.filter)
    const [name, SetName] = useState('')
    return (
        <div className="catalog_menu">
            <div id='catalog_header'>
                <div className='catalog_title'>Каталог</div>
            </div>
            <form onSubmit={(e) => {
                e.preventDefault()
                filter.name = name
                dispatch(setFilterNameActions(name))
                GetGameList(filter, dispatch)
            }}>
                <input type='text' placeholder='Поиск' id='catalog_input' onChange={
                    (e) => {SetName(e.target.value)}
                }/>
            </form>
            <CatalogList/>
        </div>
    )
}
import { useSelector, useDispatch } from "react-redux"
import { GetGameList } from "../../services/BackApi"
import { changeShowGenresActions } 
    from "../../store/catalogFilterReducer"
import ChooseGenreButton from "../common/ChooseGenreButton"

function AllGenres() {
    const dispatch = useDispatch()
    const filter = useSelector(state => state.catalog.filter)
    const show = filter.show_all
    const genres = filter.genres
    const Handler = () => {
        dispatch(changeShowGenresActions())
        GetGameList(filter, dispatch)
    }
    if (!show)
        return (
            <div></div>
        )
    return (
        <div>
            <div className="menu">
                {genres.map((elem, index) => 
                    <ChooseGenreButton key={'filter_genre'+elem} text={elem} index={index}/>)}
                <button onClick={Handler} id='genre_choose_button'>Выбрать</button>
            </div>
        </div>
    )
}

export default function FilterGenres() {
    const dispatch = useDispatch()
    const choose_style = {
        color: 'grey'
    }
    return (
        <div className="filter_genres">
            <button className="genre_name" onClick={()=>dispatch(changeShowGenresActions())}>
                <div>Жанры</div>
                <div style={choose_style}>{'Выбрать'}</div>
            </button>
            <AllGenres/>
        </div>
    )
}
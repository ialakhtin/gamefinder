import { useDispatch } from "react-redux"

export default function FilterRange(props) {
    var start = 1000, end = -1
    const dispatch = useDispatch()
    const handleStart = event => {
        start = +event.target.value
        if (isNaN(start))
            start = 1000
        dispatch(props.action(start, end))
    }
    const handleEnd = event => {
        end = +event.target.value
        if (isNaN(end))
            end = -1
        dispatch(props.action(start, end))
    }
    return (
        <div className="filter_range">
            <p>{props.title}</p>
            <div>
                <input onKeyUp={handleStart} placeholder='От'></input>
                <hr/>
                <input onKeyUp={handleEnd} placeholder='До'></input>
            </div>
        </div>
    )
}
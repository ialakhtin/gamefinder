import { useSelector, useDispatch } from "react-redux"
import { setCurrentOrderActions, showOrdersActions, changeShowOrdersActions } 
    from "../../store/catalogMenuReducer"

function AllOrders() {
    const show = useSelector(state => state.catalog.menu.show_all)
    const orders = useSelector(state => state.catalog.menu.orders)
    const dispatch = useDispatch()
    if (!show)
        return (
            <div></div>
        )
    const handler = (name) => {
        dispatch(setCurrentOrderActions(name))
        dispatch(showOrdersActions(false))
    }
    return (
        <div className="menu">
            {orders.map(elem => 
                <button className="order_name" key={'order'+elem} onClick={()=>handler(elem)}>
                    {elem}
                </button>)}
        </div>
    )
}

export default function CatalogOrder() {
    const current = useSelector(state => state.catalog.menu.current_order)
    const dispatch = useDispatch()
    return (
        <div className="catalog_order">
            <button className="order_name" onClick={()=>dispatch(changeShowOrdersActions())}>
                {current}
            </button>
            <AllOrders/>
        </div>
    )
}
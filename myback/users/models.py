from django.db import models
from django.contrib.auth.models import AbstractUser

def upload_to(instance, filename):
    return 'avatars/{filename}'.format(filename=filename)

class CustomUser(AbstractUser):
    avatar = models.ImageField(upload_to=upload_to, default='avatars/default_user.png', blank=True, null=True)
    def __str__(self):
        return f'{self.username}'
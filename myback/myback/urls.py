from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('auth_api/token', jwt_views.TokenObtainPairView.as_view()),
    path('auth_api/refresh', jwt_views.TokenRefreshView.as_view())
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

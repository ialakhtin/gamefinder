from django.contrib import admin
from .models import *

admin.site.register(Game)
admin.site.register(Genre)
admin.site.register(Favorite)
admin.site.register(GameGenre)
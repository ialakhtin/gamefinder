from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status, generics
from .serializers import *
from rest_framework.status import *
from rest_framework_simplejwt.tokens import RefreshToken

class FavoriteList(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.data.get('user_id')
        favorites = Game.objects.filter(favorite__user__id=user)
        serializer = GameSerializer(favorites, many=True)
        return Response(serializer.data)

class FavoriteDetil(APIView):
    permission_classes = [IsAuthenticated]
    def post(self, request):
        data = {
            'user': request.data.get('user_id'),
            'game': request.data.get('game_id')
        }
        serializer = FavoriteSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)

    def delete(self, request):
        user = request.data.get('user_id')
        game = request.data.get('game_id')
        favorites = Favorite.objects.filter(user=user) & Favorite.objects.filter(game=game)
        favorites.delete()
        return Response(status=status.HTTP_200_OK)

class IsFavorite(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.data.get('user_id')
        game = request.data.get('game_id')
        favorites = Favorite.objects.filter(user=user) & Favorite.objects.filter(game=game)
        serializer = FavoriteSerializer(favorites, many=True)
        return Response(serializer.data)

class UserCreate(APIView):
    def post(self, request):
        if CustomUser.objects.filter(username=request.data.get("username")).count():
            return Response(status=HTTP_403_FORBIDDEN)

        user = CustomUser.objects.create_user(
            username=request.data.get("username"),
            email=request.data.get("email"),
            password=request.data.get("password"))

        refresh = RefreshToken.for_user(user=user)
        access = refresh.access_token
        return Response({
            "access": str(access),
            "refresh": str(refresh)
            }, status=HTTP_200_OK)


class UserDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer
    queryset = CustomUser.objects.all()

class GameDetail(generics.RetrieveAPIView):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

class GameList(APIView):
    def post(self, request):
        self.request = request
        queryset = self.get_queryset()
        serializer = GameSerializer(queryset, many=True)
        return Response(serializer.data)

    def recalc(self, start, end):
        if start > end:
            if end != -1:
                start = end
            elif start != 1000:
                end = start
        return (start, end)

    def get_queryset(self):
        name = self.request.data.get('name')
        genres = self.request.data.get('genres')
        choosen_genres = self.request.data.get('choosen_genres')
        players = self.request.data.get('player_count')
        time = self.request.data.get('play_time')
        p_start = players.get('start')
        p_end = players.get('end')
        t_start = time.get('start')
        t_end = time.get('end')
        p_start, p_end = self.recalc(p_start, p_end)
        t_start, t_end = self.recalc(t_start, t_end)

        queryset = Game.objects.filter(players_start__lte=p_start) &\
            Game.objects.filter(players_end__gte=p_end) &\
            Game.objects.filter(time_start__lte=t_start) &\
            Game.objects.filter(time_end__gte=t_end)
        if name != '':
            pattern = '^'
            for c in name:
                pattern += f'[{c.lower()}{c.upper()}]'
            queryset = queryset & Game.objects.filter(title__iregex=pattern)
        for i in range(len(genres)):
            if choosen_genres[i] == 0:
                continue
            genre_id = Genre.objects.get(genre=genres[i]).id
            if choosen_genres[i] == 1:
                queryset = queryset & Game.objects.filter(genres__genre__id=genre_id)
            else:
                queryset = queryset & Game.objects.exclude(genres__genre__id=genre_id)
        return queryset

class GenreList(APIView):
    def post(self, request):
        genres = Genre.objects.filter(games__game__id=request.data.get('game_id'))
        serializer = GenreSerializer(genres, many=True)
        return Response(serializer.data)

class AllGenres(generics.ListAPIView):
    serializer_class = GenreSerializer
    queryset = Genre.objects.all()
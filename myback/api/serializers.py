from .models import *
from rest_framework import serializers

class GameGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameGenre
        fields = '__all__'

class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorite
        fields = ['game', 'user']

class GenreSerializer(serializers.ModelSerializer):
    games = GameGenreSerializer(many=True, read_only=True)
    class Meta:
        model = Genre
        fields = ['genre', 'games']

class UserSerializer(serializers.ModelSerializer):
    favorites = FavoriteSerializer(many=True, read_only=True)
    class Meta:
        model = CustomUser
        fields = ['username', 'avatar', 'favorites']

class GameSerializer(serializers.ModelSerializer):
    genres = GameGenreSerializer(many=True, read_only=True)
    favorites = FavoriteSerializer(many=True, read_only=True)
    class Meta:
        model = Game
        fields = ['id', 'title', 'text', 'icon', 'genres', 'favorites']
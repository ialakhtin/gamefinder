from django.db import models
from users.models import CustomUser

class Game(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField(max_length=300)
    players_start = models.IntegerField()
    players_end = models.IntegerField()
    time_start = models.IntegerField()
    time_end = models.IntegerField()
    icon = models.ImageField(upload_to='games/')

    def __str__(self):
        return f'{self.title}'

class Favorite(models.Model):
    user = models.ForeignKey(CustomUser, related_name='favorites', on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)

class Genre(models.Model):
    genre = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.genre}'

class GameGenre(models.Model):
    game = models.ForeignKey(Game, related_name='genres', on_delete=models.CASCADE)
    genre = models.ForeignKey(Genre, related_name='games', on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.game}: {self.genre}'

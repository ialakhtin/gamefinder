from django.urls import path
from .views import *

urlpatterns = [
    path('favorite_list/', FavoriteList.as_view()),
    path('favorite_detail/', FavoriteDetil.as_view()),
    path('is_favorite/', IsFavorite.as_view()),
    path('user_create/', UserCreate.as_view()),
    path('user_detail/<int:pk>', UserDetail.as_view()),
    path('game_detail/<int:pk>', GameDetail.as_view()),
    path('game_list/', GameList.as_view()),
    path('genre_list/', GenreList.as_view()),
    path('all_genres/', AllGenres.as_view()),
]
